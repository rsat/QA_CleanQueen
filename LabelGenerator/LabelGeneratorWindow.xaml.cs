﻿using DataMatrix.net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace LabelGenerator
{
    /// <summary>
    /// Lógica de interacción para LabelGeneratorWindow.xaml
    /// </summary>
    public partial class LabelGeneratorWindow : Window
    {
        Order order;
        public LabelGeneratorWindow(Order order)
        {
            InitializeComponent();
            this.order = order;
            orderInfo.Text = order.shipping.first_name + " " + order.shipping.last_name + " - " + order.shipping.address_1;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            MessageBoxResult rsltMessageBox = MessageBox.Show("¿Está seguro?" , "Creación de Label", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);


            switch (rsltMessageBox)
            {
                case MessageBoxResult.No:
                    return;
                    break;

                case MessageBoxResult.Cancel:
                    return;
                    break;
            }
            int nBoxes = int.Parse(boxesCount.Text);
            int nBags = int.Parse(bagsCount.Text);
            int nPaperPackages = int.Parse(paperPackagesCount.Text);
            int nBroomSticks = int.Parse(broomSticksCount.Text);

            int packagesCount = nBoxes + nBags + nPaperPackages + nBroomSticks;

            string orderId = order.id.ToString();
            //datamatrix

            int maxX, maxY;

            int packageNumber = 1;
            while (packageNumber <= packagesCount)
            {
                Bitmap b = new Bitmap("cleanqueen_label_template.png");

                Graphics graphics = Graphics.FromImage(b);
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                System.Drawing.Brush brush = System.Drawing.Brushes.Black;

                PrivateFontCollection fontCollection = new PrivateFontCollection();
                fontCollection.AddFontFile(@".\helveticaneue-medium.ttf");
                fontCollection.AddFontFile(@".\helvetica-lt.ttf");
                System.Drawing.FontFamily fontFamily = new System.Drawing.FontFamily("HelveticaNeue", fontCollection);
                System.Drawing.FontFamily helveticaLT = new System.Drawing.FontFamily("Helvetica LT", fontCollection);

                Font orderFont = new Font(fontFamily, 55, GraphicsUnit.Point);
                int orderYFinal = 127;
                int orderXFinal = 397;
                SizeF orderSize = graphics.MeasureString(orderId, orderFont);

                float orderY = (orderYFinal - orderSize.Height) / 2;
                float orderX = (orderXFinal - orderSize.Width) / 2;
                graphics.DrawString(orderId, orderFont, brush, orderX, orderY);


                PrivateFontCollection helveticaLTBoldFontCollection = new PrivateFontCollection();

                helveticaLTBoldFontCollection.AddFontFile(@".\helvetica-lt-bold.ttf");

                fontFamily = new System.Drawing.FontFamily("Helvetica LT", fontCollection);

                float fontSize = 18;
                Font font = new Font(fontFamily, fontSize, GraphicsUnit.Pixel);

                float shippingX = 16;
                float shippingY = 235;

                graphics.DrawString(order.getETA().ToUpper(), font, brush, shippingX, shippingY + fontSize);
                System.Drawing.FontFamily helveticaBold = new System.Drawing.FontFamily("Helvetica LT", helveticaLTBoldFontCollection);

                font = new Font(helveticaBold, fontSize, GraphicsUnit.Pixel);

                graphics.DrawString(order.getShippingMethod(), font, brush, shippingX, shippingY);


                fontFamily = new System.Drawing.FontFamily("HelveticaNeue", fontCollection);
                font = new Font(fontFamily, 45);
                float packageNumberY = shippingY + 75;
                float packageNumberX = shippingX + 270;
                graphics.DrawString(packagesCount.ToString(), font, brush, packageNumberX, packageNumberY);
                graphics.DrawString(packageNumber.ToString(), font, brush, packageNumberX - 125, packageNumberY);



                fontSize = 20;
                Font nameFont = new Font(helveticaBold, fontSize, GraphicsUnit.Pixel);
                Font addressFont = new Font(helveticaLT, fontSize, GraphicsUnit.Pixel);

                String personName = order.shipping.first_name + ' ' + order.shipping.last_name;
                String address1 = order.shipping.address_1;
                String address2 = order.shipping.address_2;
                String city = order.shipping.city;
                

                float personInfoY = shippingY + 210;
                float personInfoX = shippingX;
                graphics.DrawString(personName.ToUpper(), nameFont, brush, personInfoX,personInfoY);
                graphics.DrawString(address1.ToUpper(), addressFont, brush, personInfoX, personInfoY + fontSize );
                graphics.DrawString(address2.ToUpper(), addressFont, brush, personInfoX, personInfoY+ fontSize * 2);
                graphics.DrawString(city.ToUpper(), nameFont, brush, personInfoX, personInfoY + fontSize * 3);


                //draw the datamatrix
                DataMatrix.net.DmtxImageEncoder encoder = new DataMatrix.net.DmtxImageEncoder();
                DmtxImageEncoderOptions options = new DmtxImageEncoderOptions();
                options.SizeIdx = DmtxSymbolSize.DmtxSymbol20x20;
                options.ModuleSize = 7;
                Bitmap bmp = encoder.EncodeImage(orderId + '-' + packageNumber, options);

                graphics.DrawImage(bmp, 0 +7, 235+70);

                String imageFilePath = orderId + "-" + packageNumber.ToString() + ".png";
                b.Save(imageFilePath, ImageFormat.Png);
                packageNumber++;
            }

            String message = "Se crearon " + packagesCount.ToString() + " archivos png";
            MessageBox.Show(message);
        }

        public interface OnChildWindowInteraction{
            void OnOrderFinish();
            
        }

        private void onFinish(object sender, RoutedEventArgs e)
        {
            this.Close();

        }
    }
}
