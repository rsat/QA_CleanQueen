﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using DataMatrix.net;
using System.Drawing.Imaging;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Threading;

namespace LabelGenerator
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Order order;
        ObservableCollection<Order> orders = new ObservableCollection<Order>();
        ObservableCollection<Order> awaitingShipmentOrders = new ObservableCollection<Order>();
        HttpClient client;
        public MainWindow()
        {
            InitializeComponent();
            printButton.Visibility = Visibility.Hidden;
            changeStateButton.Visibility = Visibility.Hidden;
            editButton.Visibility = Visibility.Hidden;

            ordersList.ItemsSource = orders;
            awaitingShipmentOrdersList.ItemsSource = awaitingShipmentOrders;

            //DEBUG CODE
            /*using (StreamReader r = new StreamReader( "22195.json"))
            {
                string json = r.ReadToEnd();
                Order deserializedOrder = JsonConvert.DeserializeObject<Order>(json);
                deserializedOrder.status = "awaiting-shipment";
                awaitingShipmentOrders.Add(deserializedOrder);
                for (int i = 0; i < 23; i++)
                {
                    deserializedOrder.id = i;
                    Order o = new Order();
                    o.status = "processing";
                    o.id = i;
                    Shipping shipping = new Shipping();
                    shipping.address_1 = "null";
                    o.shipping = shipping;
                    orders.Add(o);
                }
            }
            using (StreamReader r = new StreamReader("22180.json"))
            {
                string json = r.ReadToEnd();
                Order deserializedOrder = JsonConvert.DeserializeObject<Order>(json);
                orders.Add(deserializedOrder);
            }*/

            //configure the htpp client

            client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("ck_2e2513611ff8616064f084171a9141f861845c3f:cs_ea3cb461e6b5accdcf0b6f0f1b700d4d73efcadc");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            loadOrders();
            
        }

        

        async void loadOrders()
        {
            var response = await client.GetAsync("https://dev.cleanqueen.cl/wp-json/wc/v3/orders/");
            string result = response.Content.ReadAsStringAsync().Result;
            List<Order> serverOrders = JsonConvert.DeserializeObject<List<Order>>(result);

            foreach (Order order in serverOrders)
            {
                if (order.status == "processing")
                {
                    orders.Add(order);
                }
            }
        }

        async Task<Order> loadOrder(int orderId)
        {
            string url = "https://dev.cleanqueen.cl/wp-json/wc/v3/orders/" + orderId;
            var response = await client.GetAsync(url);
            string result = response.Content.ReadAsStringAsync().Result;
            Order fetchedOrder = JsonConvert.DeserializeObject<Order>(result);
            return fetchedOrder;
        }

        async void requestChangeStateToAwaitingShipment(int orderId)
        {
            StringContent json = new StringContent("{\"status\": \"awaiting-shipment\"}", Encoding.UTF8, "application/json");
            string url = "https://dev.cleanqueen.cl/wp-json/wc/v3/orders/" + orderId;
            var response = await client.PostAsync(url, json);

            string result = response.Content.ReadAsStringAsync().Result;

        }


        private async void loadOrderButtonClick(object sender, RoutedEventArgs e)
        {
            int selectedOrderId = int.Parse(orderTextInput.Text);

            order = await loadOrder(selectedOrderId);

            if(order.id == 0)
            {

                MessageBox.Show("Id inválido");
                return;
            }

            ordersList.SelectedIndex = -1;
            awaitingShipmentOrdersList.SelectedIndex = -1;

            setSceneForOrder();

        }



        void printWithDialog()
        {
            MessageBoxResult rsltMessageBox = MessageBox.Show("¿Desea imprimir los label?", "Creación de Label", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

            switch (rsltMessageBox)
            {
                case MessageBoxResult.No:
                    return;
                    break;

                case MessageBoxResult.Cancel:
                    return;
                    break;
            }
            int nBoxes = int.Parse(boxesCount.Text);
            int nBags = int.Parse(bagsCount.Text);
            int nPaperPackages = int.Parse(paperPackagesCount.Text);
            int nBroomSticks = int.Parse(broomSticksCount.Text);

            int packagesCount = nBoxes + nBags + nPaperPackages + nBroomSticks;

            string orderId = order.id.ToString();
            //datamatrix

            int maxX, maxY;

            int packageNumber = 1;
            while (packageNumber <= packagesCount)
            {
                Bitmap b = new Bitmap("cleanqueen_label_template.png");

                Graphics graphics = Graphics.FromImage(b);
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                System.Drawing.Brush brush = System.Drawing.Brushes.Black;

                PrivateFontCollection fontCollection = new PrivateFontCollection();
                fontCollection.AddFontFile(@".\helveticaneue-medium.ttf");
                fontCollection.AddFontFile(@".\helvetica-lt.ttf");
                System.Drawing.FontFamily fontFamily = new System.Drawing.FontFamily("HelveticaNeue", fontCollection);
                System.Drawing.FontFamily helveticaLT = new System.Drawing.FontFamily("Helvetica LT", fontCollection);

                Font orderFont = new Font(fontFamily, 55, GraphicsUnit.Point);
                int orderYFinal = 127;
                int orderXFinal = 397;
                SizeF orderSize = graphics.MeasureString(orderId, orderFont);

                float orderY = (orderYFinal - orderSize.Height) / 2;
                float orderX = (orderXFinal - orderSize.Width) / 2;
                graphics.DrawString(orderId, orderFont, brush, orderX, orderY);


                PrivateFontCollection helveticaLTBoldFontCollection = new PrivateFontCollection();

                helveticaLTBoldFontCollection.AddFontFile(@".\helvetica-lt-bold.ttf");

                fontFamily = new System.Drawing.FontFamily("Helvetica LT", fontCollection);

                float fontSize = 18;
                Font font = new Font(fontFamily, fontSize, GraphicsUnit.Pixel);

                float shippingX = 16;
                float shippingY = 235;

                graphics.DrawString(order.getETA().ToUpper(), font, brush, shippingX, shippingY + fontSize);
                System.Drawing.FontFamily helveticaBold = new System.Drawing.FontFamily("Helvetica LT", helveticaLTBoldFontCollection);

                font = new Font(helveticaBold, fontSize, GraphicsUnit.Pixel);

                graphics.DrawString(order.getShippingMethod(), font, brush, shippingX, shippingY);


                fontFamily = new System.Drawing.FontFamily("HelveticaNeue", fontCollection);
                font = new Font(fontFamily, 45);
                float packageNumberY = shippingY + 75;
                float packageNumberX = shippingX + 270;
                graphics.DrawString(packagesCount.ToString(), font, brush, packageNumberX, packageNumberY);
                graphics.DrawString(packageNumber.ToString(), font, brush, packageNumberX - 125, packageNumberY);



                fontSize = 20;
                Font nameFont = new Font(helveticaBold, fontSize, GraphicsUnit.Pixel);
                Font addressFont = new Font(helveticaLT, fontSize, GraphicsUnit.Pixel);

                String personName = order.shipping.first_name + ' ' + order.shipping.last_name;
                String address1 = order.shipping.address_1;
                String address2 = order.shipping.address_2;
                String city = order.shipping.city;


                float personInfoY = shippingY + 210;
                float personInfoX = shippingX;
                graphics.DrawString(personName.ToUpper(), nameFont, brush, personInfoX, personInfoY);
                graphics.DrawString(address1.ToUpper(), addressFont, brush, personInfoX, personInfoY + fontSize);
                graphics.DrawString(address2.ToUpper(), addressFont, brush, personInfoX, personInfoY + fontSize * 2);
                graphics.DrawString(city.ToUpper(), nameFont, brush, personInfoX, personInfoY + fontSize * 3);


                //draw the datamatrix
                DataMatrix.net.DmtxImageEncoder encoder = new DataMatrix.net.DmtxImageEncoder();
                DmtxImageEncoderOptions options = new DmtxImageEncoderOptions();
                options.SizeIdx = DmtxSymbolSize.DmtxSymbol20x20;
                options.ModuleSize = 7;
                Bitmap bmp = encoder.EncodeImage(orderId + '-' + packageNumber, options);

                graphics.DrawImage(bmp, 0 + 7, 235 + 70);

                String imageFilePath = orderId + "-" + packageNumber.ToString() + ".png";
                b.Save(imageFilePath, ImageFormat.Png);
                packageNumber++;
            }

            String message = "Se crearon " + packagesCount.ToString() + " archivos png";
            MessageBox.Show(message);

        }
        private void printButtonClick(object sender, RoutedEventArgs e)
        {
            printWithDialog();
        }

        private async void changeStateToAwaitingShipmentButtonClick(object sender, RoutedEventArgs e)
        {

            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("ck_2e2513611ff8616064f084171a9141f861845c3f:cs_ea3cb461e6b5accdcf0b6f0f1b700d4d73efcadc");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            StringContent json = new StringContent("{\"status\": \"awaiting-shipment\"}", Encoding.UTF8, "application/json");
        

            var response = await client.PostAsync("https://dev.cleanqueen.cl/wp-json/wc/v3/orders/16614", json);

            var responseGET = await client.GetAsync("https://dev.cleanqueen.cl/wp-json/wc/v3/orders/");
            string resultget = responseGET.Content.ReadAsStringAsync().Result;
            string result = response.Content.ReadAsStringAsync().Result;
            Console.WriteLine(result);
            ;
        }
   
        
       

        private async void ordersListGotFocus(object sender, EventArgs e)
        {
            ordersList.SelectedIndex = -1;
            return;
            int selectedIndex = ordersList.SelectedIndex;
            if (selectedIndex < 0)
            {
                return;
            }


            Order selectedOrder = orders[selectedIndex];
            order = await loadOrder(selectedOrder.id);
            setSceneForOrder();
        }



        private void awaitingShipmentOrdersListGotFocus(object sender, EventArgs e)
        {
            awaitingShipmentOrdersList.SelectedIndex = 1;
            return ;
            int selectedIndex = ((ListBox)sender).SelectedIndex;
            if(selectedIndex < 0)
            {
                return;
            }

            order = awaitingShipmentOrders[selectedIndex];

            orderId.Text = order.id.ToString();
            orderStatus.Content = order.status;
            shipmentAddress.Content = order.shipping.address_1;

            boxesCount.IsEnabled = false;
            bagsCount.IsEnabled = false;
            paperPackagesCount.IsEnabled = false;
            broomSticksCount.IsEnabled = false;


            boxesCount.Text = order.nBoxes.ToString();
            bagsCount.Text = order.nBags.ToString();
            paperPackagesCount.Text = order.nPaperPackages.ToString();
            broomSticksCount.Text = order.nBroomsticks.ToString();

            printButton.Visibility = Visibility.Visible;
            printButton.Content = "ReImprimir";
            editButton.Visibility = Visibility.Visible;
            ;

        }

        void setSceneForOrder()
        {

            orderId.Text = order.id.ToString();
            orderStatus.Content = order.status;
            shipmentAddress.Content = order.shipping.address_1;

            if(order.status == "processing")
            {
                boxesCount.IsEnabled = true;
                bagsCount.IsEnabled = true;
                paperPackagesCount.IsEnabled = true;
                broomSticksCount.IsEnabled = true;

                boxesCount.Text = "";
                bagsCount.Text = "";
                paperPackagesCount.Text = "";
                broomSticksCount.Text = "";


                printButton.Visibility = Visibility.Hidden;
                editButton.Visibility = Visibility.Hidden;

                Application.Current.Dispatcher.BeginInvoke(
                        new Action(() =>
                        {
                            Keyboard.Focus(boxesCount);
                        }),
                        DispatcherPriority.ContextIdle,
                        null
                    );

                ;


            }
            else {

                boxesCount.IsEnabled = false;
                bagsCount.IsEnabled = false;
                paperPackagesCount.IsEnabled = false;
                broomSticksCount.IsEnabled = false;

                boxesCount.Text = order.nBoxes.ToString();
                bagsCount.Text = order.nBags.ToString();
                paperPackagesCount.Text = order.nPaperPackages.ToString();
                broomSticksCount.Text = order.nBroomsticks.ToString();

                printButton.Visibility = Visibility.Visible;
                printButton.Content = "ReImprimir";
                editButton.Visibility = Visibility.Visible;

            }
        }

        private async void ordersListSelectionChanged(object sender, EventArgs e)
        {

            int selectedIndex = ordersList.SelectedIndex;
            if (selectedIndex == -1)
            {
                return;
            }

            awaitingShipmentOrdersList.SelectedIndex = -1;

            Order selectedOrder = orders[selectedIndex];
            order = await loadOrder(selectedOrder.id);


            setSceneForOrder();


        }


        private async void awaitingShipmentOrdersListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            int selectedIndex = awaitingShipmentOrdersList.SelectedIndex;
            if (selectedIndex == -1)
            {
                return;
            }

            ordersList.SelectedIndex = -1;
            Order selectedOrder = awaitingShipmentOrders[selectedIndex];
            order = selectedOrder;
            //order = await loadOrder(selectedOrder.id);

            setSceneForOrder();


        }


        private void packagesCountKeyDown(object sender, KeyEventArgs e)
        {

            var isNumber = e.Key >= Key.D0 && e.Key <= Key.D9;

            if (!isNumber)
            {
                return;
            }
            var request = new TraversalRequest(FocusNavigationDirection.Next);
            request.Wrapped = true;
            ((TextBox)sender).MoveFocus(request);
        }

        int collectionIndexOfOrder(ObservableCollection<Order> collection, Order o)
        {
            Order foundOrder = collection.FirstOrDefault(order => order.id == o.id);

            if(foundOrder == default(Order)){

                return -1;
            }

            return collection.IndexOf(foundOrder);
        }


        private void finalPackagesCountKeyDown(object sender, KeyEventArgs e)
        {
            var isNumber = e.Key >= Key.D0 && e.Key <= Key.D9;
            if (!isNumber)
            {
                return;
            }

            string msg = "¿Cambiar el estado a 'Esperando envío'?";
            if (order.status != "processing")
            {
                msg = "Desea enviar los nuevos cambios?";
            }
            
            MessageBoxResult wantToUpdateStatus = MessageBox.Show(msg, "Cambio de estado", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

            switch (wantToUpdateStatus)
            {
                case MessageBoxResult.Yes:

                    order.nBoxes = int.Parse(boxesCount.Text);
                    order.nBags = int.Parse(bagsCount.Text);
                    order.nPaperPackages = int.Parse(paperPackagesCount.Text);
                    order.nBroomsticks = int.Parse(broomSticksCount.Text);
                    ;

                    order.status = "awaiting-shipment";
                    requestChangeStateToAwaitingShipment(order.id);

                    if (collectionIndexOfOrder(orders, order)!=-1)
                    {
                        Order orderToremove = orders[collectionIndexOfOrder(orders, order)];
                        orders.Remove(orderToremove);
                        
                    }
                    if (collectionIndexOfOrder(awaitingShipmentOrders, order) != -1)
                    {

                        Order orderToMove = awaitingShipmentOrders[collectionIndexOfOrder(awaitingShipmentOrders, order)];

                        int index = awaitingShipmentOrders.IndexOf(orderToMove);
                        awaitingShipmentOrders.Move(index, 0);
                        
                    }
                    else
                    { 
                       
                        awaitingShipmentOrders.Insert(0, order);
                    }


                    //Set the view for awaiting-shipment order;
                    setSceneForOrder();
                    printWithDialog();
                    break;               
            }

        }

       


        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            boxesCount.IsEnabled = true;
            bagsCount.IsEnabled = true;
            paperPackagesCount.IsEnabled = true;
            broomSticksCount.IsEnabled = true;
            boxesCount.Text = "";
            bagsCount.Text = "";
            paperPackagesCount.Text = "";
            broomSticksCount.Text = "";
            Keyboard.Focus(boxesCount);
            editButton.Visibility = Visibility.Hidden;
            printButton.Visibility = Visibility.Hidden;


        }
    }



    public class Order
    {
        public int id { get; set; }

        public int nBoxes, nBags, nPaperPackages, nBroomsticks = 0;
        public string status { get; set; }
        public Shipping shipping { get; set; }
        public List<MetaData> meta_data { get; set; }
        public List<ShippingLine> shipping_lines { get; set; }


        public string getShippingMethod() {
            return this.shipping_lines[0].method_title;
        }

        public string getETA()
        {
            string ETA = "";
            foreach (MetaData metaData in this.meta_data){
                if (metaData.key == "_delivery_date")
                {
                    ETA = (string) metaData.value.ToString();
                }
            }
            return ETA;
        }

        public override string ToString()
        {
            return this.id.ToString();
        }

    }
    public class Shipping
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }

    }
    public class MetaData
    {
        public int id { get; set; }
        public string key { get; set; }
        public object value { get; set; }

    }
    public class ShippingLine
    {
        public int id { get; set; }
        public string method_title { get; set; }
        public string method_id { get; set; }
        public string instance_id { get; set; }
        public string total { get; set; }
        public string total_tax { get; set; }
        public List<object> taxes { get; set; }
        //public List<MetaData3> meta_data { get; set; }

    }

}
